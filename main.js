// Assignment 4
const elForm = document.querySelector("#form");
const elInput = document.querySelector(".main__input");
const elList = document.querySelector(".elList");
const todoArr = [];

function renderArr(params) {
  elList.innerHTML = "";
  params.forEach(elem => {
    const elItem = document.createElement("li");
    elItem.classList.add("list__item");
    elItem.innerText = elem.text;
    elList.append(elItem);
  });
  console.log(todoArr);
}

elForm.addEventListener("submit", e => {
  e.preventDefault();
  const elInputValue = elInput.value;
  todoArr.push({
    text: elInputValue,
  });
  renderArr(todoArr);
  elInput.value = null;
});

function findDublicatedHandler(a) {
  const duplicates = a.filter((el, ind) => ind !== a.indexOf(el));
  return duplicates;
}
console.log(findDublicatedHandler([1, 2, 1, 3, 41, 2, 4, 6, 3, 7, 5, 6, 38]));

function unionHandler(a, b) {
  return a.concat(b);
}
console.log(unionHandler([1, 2], [2, 3]));

function removeFalsy(a) {
  return a.filter(a => a);
}
console.log(removeFalsy([null, 0, "", false, undefined, NaN, 34, "great", 6]));

function reverseStringHandler(str) {
  return str
    .split("")
    .sort((a, b) => a.localeCompare(b))
    .join("");
}
console.log(reverseStringHandler("Hello Bro"));
