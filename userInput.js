const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Give me a string: ", function (str) {
  // console.log(
  //   str
  //     .split(" ")
  //     .map(function (a) {
  //       return a.match(/[a-z]/) ? a.toUpperCase() : a.toLowerCase();
  //     })
  //     .join(),
  // );
  console.log(
    str
      .split(" ")
      .map(a => {
        let len = a.length;
        let result = "";
        for (let i = 0; i < len; i++) {
          let char = a[i];
          if (char === char.toLowerCase()) {
            result = result + char.toUpperCase();
          } else {
            result = result + char.toLowerCase();
          }
        }
        return result;
      })
      .join(" "),
  );
  rl.close();
});

rl.on("close", function () {
  process.exit(0);
});
